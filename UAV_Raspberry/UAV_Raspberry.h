#ifndef UAV_RASPBERRY_H
#define UAV_RASPBERRY_H

#include "UAV_Comm.h"
#include "UAV_Serial.h"
#include "MemoryStructs.h"
#include "UAV_Time.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#define ARDUINO_PING_RATE 1000
#define GROUND_STATION_HERTZ_SEND_RATE 1000
#define YAW_PITCH_ROLL_SEND_RATE 33
#define GPS_SEND_RATE 200
#define MAIN_UPDATE_HERTZ 1

class UAV_Raspberry : UAV_CommEvents, UAV_SerialEvents
{
public:
	UAV_Raspberry();
	~UAV_Raspberry();	

	//UAV_CommEvents
	void OnVideoStartStop(StartStopVideoStruct startStopVideo);
	
private:
	void Update();
	float GetSignalStrength();

	long currentTime;
	bool running;
	long lastArduinoPingSend_;
	long lastGroundStationHertzSend_;
	float frameCount_;	
};

#endif