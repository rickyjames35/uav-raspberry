#ifndef UAV_COMM_H
#define UAV_COMM_H

#include <iostream>

#include <string>
#include <list>
#include <string.h>
#include "MemoryStructs.h"
#include <fcntl.h>

#if defined(WIN32)
#include <winsock2.h>
#pragma comment(lib,"Ws2_32.lib")
#include <stdio.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

#define LISTEN_PORT 5000
#define SEND_PORT 5001
#define NETWORK_BUFFER_SIZE 128
#define TEMP_NETWORK_BUFFER_SIZE 64

class UAV_CommEvents
{
public:
	virtual void OnVideoStartStop(StartStopVideoStruct startStopVideo) = 0;
};

class UAV_Comm
{
public:		
	~UAV_Comm();
	static UAV_Comm& GetInstance();
	bool OpenConnection();
	void CloseConnection();
	void RegisterEvents(UAV_CommEvents &instance);
	std::string GetRemoteIP();

	void SendData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize);
	void Update();	

private:
	UAV_Comm();
	UAV_Comm(UAV_Comm const&);
	void operator=(UAV_Comm const&);
	
	void ReadSerial();
	bool ProcessSerialData();
	bool ProcessPacket(MemoryStruct_List::DataID_ListType dataID, int dataSize);	

	struct sockaddr_in sockInfo;
	std::list<UAV_CommEvents*> eventInstances_;
	unsigned char networkBufferSize_;
	unsigned char networkBuffer_[NETWORK_BUFFER_SIZE];
	unsigned char tempNetworkBuffer_[TEMP_NETWORK_BUFFER_SIZE];
	int socketID_;
	std::string remoteIP;
};

#endif