#ifndef UAV_Time_H
#define UAV_Time_H

#include <thread>

#if defined(WIN32)
	//#include <winsock2.h>
	#include <Windows.h>
#else
	#include <sys/time.h>
#endif

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

#if defined(WIN32)
struct timezone 
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};
#endif

class UAV_Time
{
public:	
	~UAV_Time();
	static UAV_Time& GetInstance();
	long GetTime();

private:	

	UAV_Time();
	UAV_Time(UAV_Time const&);
	void operator=(UAV_Time const&);
	timeval startTime_;

#if defined(WIN32)
	int gettimeofday(struct timeval *tv, struct timezone *tz);
#endif
};

#endif