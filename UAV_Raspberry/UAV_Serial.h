#ifndef UAV_SERIAL_H
#define UAV_SERIAL_H

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>
#include <list>
#include "MemoryStructs.h"
#include "UAV_Time.h"

#if defined(WIN32)
#include <Windows.h>
#else
#include <termios.h>
#include <unistd.h>
#endif

#define SERIAL_BUFFER_SIZE 128
#define TEMP_SERIAL_BUFFER_SIZE 64
#define SEND_DATA_BUFFER_SIZE 1024
#define ARDUINO_TIMEOUT 2000
#define TRANSFER_LIMIT 4

class UAV_SerialEvents
{
public:
	//currently there are no events
};

class UAV_Serial
{
public:
	~UAV_Serial();
	static UAV_Serial& GetInstance();
	bool OpenConnection();	
	void CloseConnection();
	void RegisterEvents(UAV_SerialEvents &instance);	
	bool IsConnected();	
	bool QueueData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize);
	void SendData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize);
	void Update();

	int rxBps_;
	int txBps_;

private:
	UAV_Serial();
	UAV_Serial(UAV_Serial const&);
	void operator=(UAV_Serial const&);	

	void ReadSerial();
	bool ProcessSerialData();
	bool ProcessPacket(MemoryStruct_List::DataID_ListType dataID, int dataSize);
	void SendQueuedData();
	void ProcessQueuedData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize);

	int fd_;
	long lastPingUpdateFromArduino_;
	int sendDataLength_;

#if defined(WIN32)
	HANDLE hSerial_;
#endif
	std::list<UAV_SerialEvents*> eventInstances_;
		
	unsigned char serialBufferSize_;
	unsigned char serialBuffer_[SERIAL_BUFFER_SIZE];
	unsigned char tempSerialBuffer_[TEMP_SERIAL_BUFFER_SIZE];
	unsigned char sendDataBuffer_[SEND_DATA_BUFFER_SIZE];
};

#endif