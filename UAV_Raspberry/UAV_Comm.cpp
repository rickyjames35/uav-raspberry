#include "UAV_Comm.h"
#include "UAV_Serial.h"

UAV_Comm::UAV_Comm()
{		
	networkBufferSize_ = 0;
	remoteIP = "0";
	sockInfo.sin_family = AF_INET;
	sockInfo.sin_port = htons(SEND_PORT);
	sockInfo.sin_addr.s_addr = inet_addr(remoteIP.c_str());
}

UAV_Comm::~UAV_Comm()
{
	
}

UAV_Comm& UAV_Comm::GetInstance()
{
	static UAV_Comm instance;
	return instance;
}

bool UAV_Comm::OpenConnection()
{
#if defined(WIN32)
	WSADATA wsaData;
	int waResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

	socketID_ = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in sockInfo;
	sockInfo.sin_family = AF_INET;
	sockInfo.sin_addr.s_addr = INADDR_ANY;
	sockInfo.sin_port = htons(LISTEN_PORT);
	
	int bindResult = bind(socketID_, (struct sockaddr *)&sockInfo, sizeof(sockInfo));
	u_long iMode=1;
#if defined(WIN32)
	int nonBlock = ioctlsocket(socketID_, FIONBIO, &iMode);
#else
	int nonBlock = fcntl(socketID_, F_SETFL, fcntl(socketID_, F_GETFL) | O_NONBLOCK) < 0;
#endif

	if (socketID_ > 0 && bindResult == 0 && nonBlock == 0)
	{

		return true;
	}
	else
	{
		std::cout << "Error binding network socket\n";
		CloseConnection();
		return false;
	}
}

void UAV_Comm::CloseConnection()
{
	#if defined(WIN32)
		closesocket(socketID_);
		WSACleanup();
	#else
		close(socketID_);
	#endif
}

void UAV_Comm::RegisterEvents(UAV_CommEvents &instance)
{
	eventInstances_.push_front(&instance);
}

std::string UAV_Comm::GetRemoteIP()
{
	return remoteIP;
}

void UAV_Comm::SendData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize)
{
	if (remoteIP.compare("0"))
	{
		unsigned char* buffer = new unsigned char[DATA_ID_SIZE + dataSize];
		unsigned char convertedDataID[1] = { (unsigned char)dataID };
		memcpy(buffer, convertedDataID, DATA_ID_SIZE);
		memcpy(buffer + DATA_ID_SIZE, data, dataSize);			
		sendto(socketID_, (char*)buffer, DATA_ID_SIZE + dataSize, 0, (struct sockaddr *) & sockInfo, sizeof (sockInfo));
		delete buffer;
	}	
}

void UAV_Comm::Update()
{
	ReadSerial();
}

void UAV_Comm::ReadSerial()
{  
	int dataSize = recv(socketID_, (char*)tempNetworkBuffer_, TEMP_NETWORK_BUFFER_SIZE, 0);
	if (dataSize > 0)
	{   
		if (networkBufferSize_ + dataSize < NETWORK_BUFFER_SIZE)
		{
			memcpy(networkBuffer_ + networkBufferSize_, tempNetworkBuffer_, dataSize);
			networkBufferSize_ += dataSize;
			ProcessSerialData(); //do something if it returns false
		}
	}
}

bool UAV_Comm::ProcessSerialData()
{
	bool packetComplete = true;
	while (networkBufferSize_ >= DATA_ID_SIZE && packetComplete)
	{		
		switch(networkBuffer_[0]) 
		{
		case MemoryStruct_List::START_STOP_VIDEO:
			packetComplete = ProcessPacket(MemoryStruct_List::START_STOP_VIDEO, sizeof(StartStopVideoStruct));
			break;
		case MemoryStruct_List::TEST_LIGHT:
			packetComplete = ProcessPacket(MemoryStruct_List::TEST_LIGHT, sizeof(TestLightStruct));
			break;
		case MemoryStruct_List::AILERON_ELEVATOR_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::AILERON_ELEVATOR_INPUT, sizeof(AileronElevatorInputStruct));
			break;
		case MemoryStruct_List::RUDDER_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::RUDDER_INPUT, sizeof(RudderInputStruct));
			break;
		case MemoryStruct_List::THROTTLE_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::THROTTLE_INPUT, sizeof(ThrottleInputStruct));
			break;
		case MemoryStruct_List::FLAPS_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::FLAPS_INPUT, sizeof(FlapsInputStruct));
			break;
		case MemoryStruct_List::ENGINE_STATUS_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::ENGINE_STATUS_INPUT, sizeof(EngineStatusInputStruct));
			break;
		case MemoryStruct_List::CONNECTION_INFO:
			packetComplete = ProcessPacket(MemoryStruct_List::CONNECTION_INFO, sizeof(ConnectionInfoStruct));
			break;
		case MemoryStruct_List::HIL_SETTINGS:
			packetComplete = ProcessPacket(MemoryStruct_List::HIL_SETTINGS, sizeof(HIL_SettingsStruct));
			break;
		case MemoryStruct_List::HIL_OUTPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::HIL_OUTPUT, sizeof(HIL_OutputStruct));
			break;
		case MemoryStruct_List::WAYPOINT:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT, sizeof(WaypointStruct));
			break;
		case MemoryStruct_List::START_WAYPOINT_UPLOAD:
			packetComplete = ProcessPacket(MemoryStruct_List::START_WAYPOINT_UPLOAD, sizeof(StartWaypointUploadStruct));
			break;
		case MemoryStruct_List::REQUEST_AUTOPILOT_MODE:
			packetComplete = ProcessPacket(MemoryStruct_List::REQUEST_AUTOPILOT_MODE, sizeof(AutoPilotModeStruct));
			break;
		case MemoryStruct_List::SET_CURRENT_WAYPOINT:
			packetComplete = ProcessPacket(MemoryStruct_List::SET_CURRENT_WAYPOINT, sizeof(CurrentWaypointStruct));
			break;
		case MemoryStruct_List::WAYPOINT_DOWNLOAD_ROUTE:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT_DOWNLOAD_ROUTE, 0);
			break;
		default:
			return false;
		}
	}
	return true;
}

bool UAV_Comm::ProcessPacket(MemoryStruct_List::DataID_ListType dataID, int dataSize)
{
	if (networkBufferSize_ >= DATA_ID_SIZE + dataSize)
	{
		if (dataID == MemoryStruct_List::CONNECTION_INFO)
		{
			remoteIP = std::to_string((int)((ConnectionInfoStruct*)(networkBuffer_ + DATA_ID_SIZE))->firstOctet) + std::string(".") + std::to_string((int)((ConnectionInfoStruct*)(networkBuffer_ + DATA_ID_SIZE))->secondOctet) + std::string(".") + std::to_string((int)((ConnectionInfoStruct*)(networkBuffer_ + DATA_ID_SIZE))->thirdOctet) + std::string(".") + std::to_string((int)((ConnectionInfoStruct*)(networkBuffer_ + DATA_ID_SIZE))->fourthOctet);
			sockInfo.sin_addr.s_addr = inet_addr(remoteIP.c_str());
			std::cout << "Remote IP set to: " << remoteIP << "\n";
		}
		else if (dataID == MemoryStruct_List::START_STOP_VIDEO)
		{
			for (std::list<UAV_CommEvents*>::iterator it = eventInstances_.begin(); it != eventInstances_.end(); ++it)						
				(*it)->OnVideoStartStop(*((StartStopVideoStruct*)(networkBuffer_ + DATA_ID_SIZE)));						
		}
		else
		{			
			UAV_Serial::GetInstance().QueueData(dataID, networkBuffer_ + DATA_ID_SIZE, dataSize);
		}

		//we ate some bytes
		networkBufferSize_ -= (DATA_ID_SIZE + dataSize);
		//shift data down
		memcpy(networkBuffer_, networkBuffer_ + (DATA_ID_SIZE + dataSize), networkBufferSize_);
		return true;
	}
	else
	{
		return false;
	}
}