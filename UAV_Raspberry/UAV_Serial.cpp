#include "UAV_Comm.h"
#include "UAV_Serial.h"

UAV_Serial::UAV_Serial()
{	
	lastPingUpdateFromArduino_ = 0;
	serialBufferSize_ = 0;	
	sendDataLength_ = 0;
	rxBps_ = 0;
	txBps_ = 0;
}

UAV_Serial::~UAV_Serial()
{
	
}

UAV_Serial& UAV_Serial::GetInstance()
{	
	static UAV_Serial instance;
	return instance;
}

bool UAV_Serial::IsConnected()
{	
	return (UAV_Time::GetInstance().GetTime() - lastPingUpdateFromArduino_ < ARDUINO_TIMEOUT) && lastPingUpdateFromArduino_ != 0;
}

bool UAV_Serial::OpenConnection()
{
#if defined(WIN32)
	hSerial_ = CreateFile("COM4", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	
	if (hSerial_ == INVALID_HANDLE_VALUE)
	{
		std::cout << "Can't connect to arduino\n";
		CloseConnection();
		return false;
	}
	else
	{
		DCB dcb = {0};
		dcb.DCBlength = sizeof(DCB);

		if (!GetCommState (hSerial_, &dcb))
		{
			std::cout << "Error getting serial port state\n";
			CloseConnection();
			return false;
		}
		else
		{
			dcb.BaudRate=230400;
			dcb.ByteSize=8;
			dcb.StopBits=ONESTOPBIT;
			dcb.Parity=NOPARITY;
			if(!SetCommState(hSerial_, &dcb))
			{
				std::cout << "Error setting serial port state\n";
				CloseConnection();
				return false;
			}
			else
			{
				COMMTIMEOUTS timeouts;

				timeouts.ReadIntervalTimeout = MAXDWORD;
				timeouts.ReadTotalTimeoutMultiplier = 0;
				timeouts.ReadTotalTimeoutConstant = 0;
				timeouts.WriteTotalTimeoutMultiplier = 1;
				timeouts.WriteTotalTimeoutConstant = 1;
				if (!SetCommTimeouts(hSerial_, &timeouts))
				{
					std::cout << "Error setting serial port timeouts\n";
					CloseConnection();
					return false;
				}
			}
		}
	}
#else
	struct termios toptions;
	fd_ = open("/dev/ttyACM0", O_RDWR | O_NOCTTY | O_NDELAY | O_SYNC );
	if (fd_ == -1)
	{
		std::cout << "Can't connect to arduino\n";
		CloseConnection();
		return false;
	}

	if (tcgetattr(fd_, &toptions) < 0) 
	{
        std::cout << "Couldn't get term attributes";
        return false;
    }

	cfsetispeed(&toptions, B230400);    // set baud rates
	cfsetospeed(&toptions, B230400);

	// 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    //toptions.c_cflag &= ~HUPCL; // disable hang-up-on-close to avoid reset

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 0;
    //toptions.c_cc[VTIME] = 20;
    
    tcsetattr(fd_, TCSANOW, &toptions);
    if( tcsetattr(fd_, TCSAFLUSH, &toptions) < 0) 
	{
        std::cout << "init_serialport: Couldn't set term attributes";
        return false;
    }
	//---------------------------------------------------------------------------------------
	/*
	fd_ = open("/dev/ttyACM0", O_RDWR | O_NOCTTY | O_SYNC);
	usleep(3500000);
	if (fd_ == -1)
	{
		std::cout << "Can't connect to arduino\n";
		CloseConnection();
		return false;
	}
	else
	{
		fcntl(fd_, F_SETFL,0);
		struct termios port_settings;      // structure to store the port settings in

		cfmakeraw(&port_settings);

		cfsetispeed(&port_settings, B57600);    // set baud rates
		cfsetospeed(&port_settings, B57600);

		//port_settings.c_lflag &= ~ICANON;
		//port_settings.c_cc[VTIME] = 10;
		//port_settings.c_cflag &= ~PARENB;    // set no parity, stop bits, data bits
		//port_settings.c_cflag &= ~CSTOPB;
		//port_settings.c_cflag &= ~CSIZE; //this line is causing problems
		//port_settings.c_cflag |= CS8;

		tcsetattr(fd_, TCSANOW, &port_settings);    // apply the settings to the port	

		if (fcntl(fd_, F_SETFL, fcntl(fd_, F_GETFL) | O_NONBLOCK) < 0)
			return false;
		
	}
	*/
#endif		

	std::cout << "Connected to arduino\n";	
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));	
	return true;
}

void UAV_Serial::CloseConnection()
{		
#if defined(WIN32)
	CloseHandle(hSerial_);
#else
	close(fd_);
#endif	
}

void UAV_Serial::RegisterEvents(UAV_SerialEvents &instance)
{
	eventInstances_.push_front(&instance);
}

bool UAV_Serial::QueueData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize)
{
	if (dataSize + DATA_ID_SIZE + sendDataLength_ < SEND_DATA_BUFFER_SIZE)
	{
		memcpy(sendDataBuffer_ + sendDataLength_, (unsigned char*)&dataID, DATA_ID_SIZE);		
		memcpy(sendDataBuffer_ + sendDataLength_ + DATA_ID_SIZE, data, dataSize);
		sendDataLength_ += dataSize + DATA_ID_SIZE;
		return true;
	}
	else
	{
		return false;
	}
}

void UAV_Serial::ProcessQueuedData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize)
{
	SendData(dataID, data, dataSize);
	//we ate some bytes
	sendDataLength_ -= (DATA_ID_SIZE + dataSize);
	//shift data down
	memcpy(sendDataBuffer_, sendDataBuffer_ + (DATA_ID_SIZE + dataSize), sendDataLength_);
}

void UAV_Serial::SendQueuedData()
{
	if (sendDataLength_ >= DATA_ID_SIZE)
	{
		switch(sendDataBuffer_[0]) 
		{
		case MemoryStruct_List::TEST_LIGHT:
			ProcessQueuedData(MemoryStruct_List::TEST_LIGHT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(TestLightStruct));			
			break;
		case MemoryStruct_List::ARDUINO_PING:
			ProcessQueuedData(MemoryStruct_List::ARDUINO_PING, sendDataBuffer_ + DATA_ID_SIZE, 0);			
			break;
		case MemoryStruct_List::AILERON_ELEVATOR_INPUT:
			ProcessQueuedData(MemoryStruct_List::AILERON_ELEVATOR_INPUT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(AileronElevatorInputStruct));			
			break;
		case MemoryStruct_List::RUDDER_INPUT:
			ProcessQueuedData(MemoryStruct_List::RUDDER_INPUT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(RudderInputStruct));			
			break;
		case MemoryStruct_List::THROTTLE_INPUT:
			ProcessQueuedData(MemoryStruct_List::THROTTLE_INPUT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(ThrottleInputStruct));			
			break;
		case MemoryStruct_List::FLAPS_INPUT:
			ProcessQueuedData(MemoryStruct_List::FLAPS_INPUT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(FlapsInputStruct));			
			break;
		case MemoryStruct_List::ENGINE_STATUS_INPUT:
			ProcessQueuedData(MemoryStruct_List::ENGINE_STATUS_INPUT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(EngineStatusInputStruct));			
			break;
		case MemoryStruct_List::HIL_SETTINGS:
			ProcessQueuedData(MemoryStruct_List::HIL_SETTINGS, sendDataBuffer_ + DATA_ID_SIZE, sizeof(HIL_SettingsStruct));			
			break;
		case MemoryStruct_List::HIL_OUTPUT:
			ProcessQueuedData(MemoryStruct_List::HIL_OUTPUT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(HIL_OutputStruct));			
			break;
		case MemoryStruct_List::WAYPOINT:
			ProcessQueuedData(MemoryStruct_List::WAYPOINT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(WaypointStruct));			
			break;
		case MemoryStruct_List::START_WAYPOINT_UPLOAD:
			ProcessQueuedData(MemoryStruct_List::START_WAYPOINT_UPLOAD, sendDataBuffer_ + DATA_ID_SIZE, sizeof(StartWaypointUploadStruct));			
			break;
		case MemoryStruct_List::REQUEST_AUTOPILOT_MODE:
			ProcessQueuedData(MemoryStruct_List::REQUEST_AUTOPILOT_MODE, sendDataBuffer_ + DATA_ID_SIZE, sizeof(AutoPilotModeStruct));			
			break;
		case MemoryStruct_List::SET_CURRENT_WAYPOINT:
			ProcessQueuedData(MemoryStruct_List::SET_CURRENT_WAYPOINT, sendDataBuffer_ + DATA_ID_SIZE, sizeof(CurrentWaypointStruct));			
			break;
		case MemoryStruct_List::WAYPOINT_DOWNLOAD_ROUTE:
			ProcessQueuedData(MemoryStruct_List::WAYPOINT_DOWNLOAD_ROUTE, sendDataBuffer_ + DATA_ID_SIZE, 0);			
			break;
		default:
			std::cout << "Error: Bad queued data ID\n";
			sendDataLength_ = 0;
		}
	}
}

void UAV_Serial::SendData(MemoryStruct_List::DataID_ListType dataID, unsigned char* data, int dataSize)
{
	if (dataID == MemoryStruct_List::ARDUINO_PING || IsConnected())
	{				
#if defined(WIN32)
		DWORD bytesWritten = 0;
		WriteFile(hSerial_, (unsigned char*)&dataID, DATA_ID_SIZE, &bytesWritten, NULL);
		//FlushFileBuffers(hSerial_);
		std::this_thread::sleep_for(std::chrono::microseconds(100));
		for (int i = 0; i < dataSize; i += TRANSFER_LIMIT)
		{			
			int dataLeft = dataSize - i;
			if (dataLeft > TRANSFER_LIMIT)
				WriteFile(hSerial_, data + i, TRANSFER_LIMIT, &bytesWritten, NULL);
			else
				WriteFile(hSerial_, data + i, dataLeft, &bytesWritten, NULL);
			//FlushFileBuffers(hSerial_);
			std::this_thread::sleep_for(std::chrono::microseconds(100));			
		}		
#else
		write(fd_, (unsigned char*)&dataID, DATA_ID_SIZE);
		usleep(100);
		for (int i = 0; i < dataSize; i += TRANSFER_LIMIT)
		{			
			int dataLeft = dataSize - i;
			if (dataLeft > TRANSFER_LIMIT)
				write(fd_, data + i, TRANSFER_LIMIT);				
			else
				write(fd_, data + i, dataLeft);				
			usleep(100);
		}			
#endif
		rxBps_ += ((dataSize + DATA_ID_SIZE) * 8);
	}	
}

void UAV_Serial::Update()
{			
	ReadSerial();
	SendQueuedData();
}

void UAV_Serial::ReadSerial()
{  
	
#if defined(WIN32)
	DWORD dataSize = 0;
	ReadFile(hSerial_, tempSerialBuffer_, TEMP_SERIAL_BUFFER_SIZE, &dataSize, NULL);
#else
	int dataSize = read(fd_, tempSerialBuffer_, TEMP_SERIAL_BUFFER_SIZE);
#endif

	txBps_ += (dataSize * 8);

	if (dataSize > 0 && serialBufferSize_ + dataSize < SERIAL_BUFFER_SIZE)
	{
		memcpy(serialBuffer_ + serialBufferSize_, tempSerialBuffer_, dataSize);
		serialBufferSize_ += (unsigned char)dataSize;
		if (!ProcessSerialData())
		{
			std::cout << "Serial Read Error\n";
		}
	}	
}

bool UAV_Serial::ProcessSerialData()
{
	bool packetComplete = true;
	while (serialBufferSize_ >= DATA_ID_SIZE && packetComplete)
	{		
		switch(serialBuffer_[0]) 
		{
		case MemoryStruct_List::ARDUINO_HERTZ_RATE:
			packetComplete = ProcessPacket(MemoryStruct_List::ARDUINO_HERTZ_RATE, sizeof(ArduinoUpdateHertzStruct));
			break;
		case MemoryStruct_List::YAW_PITCH_ROLL:
			packetComplete = ProcessPacket(MemoryStruct_List::YAW_PITCH_ROLL, sizeof(YawPitchRollStruct));
			break;
		case MemoryStruct_List::GPS_OUTPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::GPS_OUTPUT, sizeof(GPS_OutputStruct));
			break;
		case MemoryStruct_List::PRESSURE_TEMP_ALT:
			packetComplete = ProcessPacket(MemoryStruct_List::PRESSURE_TEMP_ALT, sizeof(PressureTempAltStruct));
			break;
		case MemoryStruct_List::RALT:
			packetComplete = ProcessPacket(MemoryStruct_List::RALT, sizeof(RALT_Struct));
			break;
		case MemoryStruct_List::VOLTAGE_CURRENT:
			packetComplete = ProcessPacket(MemoryStruct_List::VOLTAGE_CURRENT, sizeof(VoltageCurrentStruct));
			break;
		case MemoryStruct_List::WAYPOINT_UPLOAD_COMPLETE:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT_UPLOAD_COMPLETE, 0);			
			break;
		case MemoryStruct_List::WAYPOINT_UPLOAD_READY:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT_UPLOAD_READY, 0);
			break;
		case MemoryStruct_List::HIL_INPUT:
			packetComplete = ProcessPacket(MemoryStruct_List::HIL_INPUT, sizeof(HIL_InputStruct));
			break;
		case MemoryStruct_List::CURRENT_AUTOPILOT_MODE:
			packetComplete = ProcessPacket(MemoryStruct_List::CURRENT_AUTOPILOT_MODE, sizeof(AutoPilotModeStruct));
			break;
		case MemoryStruct_List::GET_CURRENT_WAYPOINT:
			packetComplete = ProcessPacket(MemoryStruct_List::GET_CURRENT_WAYPOINT, sizeof(CurrentWaypointStruct));
			break;
		case MemoryStruct_List::GET_GOTO_ITERATIONS:
			packetComplete = ProcessPacket(MemoryStruct_List::GET_GOTO_ITERATIONS, sizeof(GotoIterationsStruct));
			break;
		case MemoryStruct_List::CONSOLE_OUT:
			packetComplete = ProcessPacket(MemoryStruct_List::CONSOLE_OUT, sizeof(ConsoleStruct));
			break;
		case MemoryStruct_List::WAYPOINT:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT, sizeof(WaypointStruct));
			break;
		case MemoryStruct_List::WAYPOINT_DOWNLOAD_COMPLETE:
			packetComplete = ProcessPacket(MemoryStruct_List::WAYPOINT_DOWNLOAD_COMPLETE, 0);
			break;
		default:
			return false;
		}
	}
	
	return true;
}

bool UAV_Serial::ProcessPacket(MemoryStruct_List::DataID_ListType dataID, int dataSize)
{
	if (serialBufferSize_ >= DATA_ID_SIZE + dataSize)
	{
		if (dataID == MemoryStruct_List::ARDUINO_HERTZ_RATE)
			lastPingUpdateFromArduino_ = UAV_Time::GetInstance().GetTime();

		UAV_Comm::GetInstance().SendData(dataID, serialBuffer_ + DATA_ID_SIZE, dataSize);

		//we ate some bytes
		serialBufferSize_ -= (DATA_ID_SIZE + dataSize);
		//shift data down
		memcpy(serialBuffer_, serialBuffer_ + (DATA_ID_SIZE + dataSize), serialBufferSize_);
		return true;
	}
	else
	{
		return false;
	}
}