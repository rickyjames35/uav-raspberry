#include "UAV_Raspberry.h"

UAV_Raspberry::UAV_Raspberry()
{
	UAV_Time::GetInstance();	
	frameCount_ = 0;
	lastArduinoPingSend_ = 0;
	lastGroundStationHertzSend_ = 0;
	UAV_Comm::GetInstance().RegisterEvents(*this);
	UAV_Comm::GetInstance().OpenConnection();
	UAV_Serial::GetInstance().RegisterEvents(*this);
	UAV_Serial::GetInstance().OpenConnection();
	running = true;
	Update();
	UAV_Comm::GetInstance().CloseConnection();
	UAV_Serial::GetInstance().CloseConnection();
}

UAV_Raspberry::~UAV_Raspberry()
{
	running = false;
}

void UAV_Raspberry::Update()
{
	while (running)
	{			
		UAV_Comm::GetInstance().Update();
		UAV_Serial::GetInstance().Update();

		currentTime = UAV_Time::GetInstance().GetTime();
		if (currentTime - lastGroundStationHertzSend_ >= GROUND_STATION_HERTZ_SEND_RATE)
		{
			RaspberryPiUpdateHertzStruct* updateHertzStruct = new RaspberryPiUpdateHertzStruct();
			float secPast = ((float)(currentTime - lastGroundStationHertzSend_) / 1000.0F);
			updateHertzStruct->updateRate = frameCount_ / secPast;
			updateHertzStruct->signalStrength = GetSignalStrength();
			updateHertzStruct->rxBps = (int)(UAV_Serial::GetInstance().rxBps_ / secPast);
			updateHertzStruct->txBps = (int)(UAV_Serial::GetInstance().txBps_ / secPast);
			UAV_Serial::GetInstance().rxBps_ = 0;
			UAV_Serial::GetInstance().txBps_ = 0;

			UAV_Comm::GetInstance().SendData(MemoryStruct_List::RASPBERRY_PI_HERTZ_RATE, reinterpret_cast<unsigned char*>(updateHertzStruct), sizeof(RaspberryPiUpdateHertzStruct));
			frameCount_ = 0;
			lastGroundStationHertzSend_ = currentTime;
		}

		if (currentTime - lastArduinoPingSend_ >= ARDUINO_PING_RATE)
		{
			UAV_Serial::GetInstance().SendData(MemoryStruct_List::ARDUINO_PING, NULL, 0);
			lastArduinoPingSend_ = currentTime;
		}		

		frameCount_++;
		std::this_thread::sleep_for(std::chrono::milliseconds(MAIN_UPDATE_HERTZ));
	}
}

float UAV_Raspberry::GetSignalStrength()
{
#if defined(WIN32)
	return 0.0F;
#else
	const int MAX_BUFFER = 2048;
	std::string cmd="iwconfig wlan0";
	char buffer[MAX_BUFFER];
	FILE *stream = popen(cmd.c_str(), "r");
	if (stream){
		while (!feof(stream))
		{
			if (fgets(buffer, MAX_BUFFER, stream) != NULL)
			{				
				size_t linkQualityStart = std::string(buffer).find("Link Quality=", 0);
				if (linkQualityStart != std::string::npos)
				{
					linkQualityStart += 13;
					size_t linkQualityEnd = std::string(buffer).find("/", linkQualityStart);
					if (linkQualityEnd != std::string::npos)
					{
						char* qualityBuff = new char[linkQualityEnd - linkQualityStart];
						std::string(buffer).copy(qualityBuff, linkQualityEnd - linkQualityStart, linkQualityStart);
						int quality = atoi(qualityBuff);
						delete qualityBuff;
						return (float)quality / 70.0F;
					}
				}
					
			}
		}
		pclose(stream);
	}
	return 0.0F;
#endif
}

void UAV_Raspberry::OnVideoStartStop(StartStopVideoStruct startStopVideo)
{
#if !defined(WIN32)
	system("killall raspivid");
	system("killall nc");
	if (startStopVideo.start)
		system((std::string("raspivid -t 999999999 -w 640 -h 480 -vf -fps 31 -b 4000000 -n -o - | nc ") + UAV_Comm::GetInstance().GetRemoteIP() + std::string(" 5002&")).c_str());
#endif
}