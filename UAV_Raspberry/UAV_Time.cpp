#include "UAV_Time.h"

UAV_Time::UAV_Time()
{
	gettimeofday(&startTime_, NULL);
}

UAV_Time::~UAV_Time()
{

}

UAV_Time& UAV_Time::GetInstance()
{
	static UAV_Time instance;
	return instance;
}

#if defined(WIN32)
int UAV_Time::gettimeofday(struct timeval *tv, struct timezone *tz)
{
  FILETIME ft;
  unsigned __int64 tmpres = 0;
  static int tzflag;
 
  if (NULL != tv)
  {
    GetSystemTimeAsFileTime(&ft);
 
    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;
 
    /*converting file time to unix epoch*/
    tmpres -= DELTA_EPOCH_IN_MICROSECS; 
    tmpres /= 10;  /*convert into microseconds*/
    tv->tv_sec = (long)(tmpres / 1000000UL);
    tv->tv_usec = (long)(tmpres % 1000000UL);
  }
 
  if (NULL != tz)
  {
    if (!tzflag)
    {
      _tzset();
      tzflag++;
    }

    tz->tz_minuteswest = _timezone / 60;
    tz->tz_dsttime = _daylight;
  }
 
  return 0;
}
#endif

long UAV_Time::GetTime()
{
	timeval currentTime;
	gettimeofday(&currentTime, NULL);
	double timeDiff = ((currentTime.tv_sec * 1000) - (startTime_.tv_sec * 1000)) + (((double)currentTime.tv_usec / 1000.0) - ((double)startTime_.tv_usec / 1000.0));
	return (long)timeDiff;
}